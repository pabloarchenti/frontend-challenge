#!/bin/bash

export PORT=3001
COMPOSE_FILE=./e2e-tests.yml

docker-compose -f $COMPOSE_FILE up -d --build frontend-challenge
docker-compose -f $COMPOSE_FILE pull test-cafe
docker-compose -f $COMPOSE_FILE up test-cafe
docker-compose -f $COMPOSE_FILE rm --force --stop -v
