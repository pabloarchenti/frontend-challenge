# Frontend challenge

## Start web server

### Locally

```sh
npm install
npm run build
npm start
```

*Default port: 3000*

#### With Docker

```sh
npm run start:with-docker
```
- *Default port: 3001*
- *Change listening port setting environment variable "PORT"*

## Run e2e tests

#### Locally

```sh
npm run test:headless
```
- *You need to start the server first.*
- *To change site url set environment variable "BASE_URL" (defaults to "http://localhost:3000")*

#### With Docker

```sh
npm run test:with-docker
```

### Some features

* Server-side rendering.
* Inline CSS.
* Lazy loading of images.

### Main dependencies
* React/Redux
* Next.js
* Material UI
* Express
* TestCafe
