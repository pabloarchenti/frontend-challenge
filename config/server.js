module.exports = {
  port: process.env.PORT || 3000,
  newsApi: {
    url: 'https://newsapi.org/v2/top-headlines',
    country: 'us',
    key: '6585daede3f64cc1bcedfb9ab6de8f91',
    maxCache: 60 * 5
  }
};
