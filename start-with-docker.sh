#!/bin/bash

PORT=${PORT:-3001}

docker build -t frontend-challenge .
docker run --rm -p "$PORT:$PORT" -e "PORT=$PORT" frontend-challenge