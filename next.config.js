const withOffline = require('next-offline');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
});
const serverRuntimeConfig = require('./config/server');
const publicRuntimeConfig = require('./config/public');

const config = {
  webpack(config) {
    process.env.BABEL_ENV = config.name;
    return config;
  },
  distDir: '../.build',
  serverRuntimeConfig,
  publicRuntimeConfig
};

module.exports = withBundleAnalyzer(
  withOffline(config)
);
