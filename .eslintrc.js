module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    "import/no-extraneous-dependencies": 0,
    "comma-dangle": 0,
    "no-underscore-dangle": 0,
    "no-param-reassign": 0,
    "strict": 0,
    "import/prefer-default-export": 0,
    "react/jsx-one-expression-per-line": 0,
    "arrow-body-style": 0,
    "object-curly-newline": 0,
    "no-shadow": 0,
    "react/prefer-stateless-function": 0,
    "template-tag-spacing": 0,
    "no-plusplus": 0
  },
};
