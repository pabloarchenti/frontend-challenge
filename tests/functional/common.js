export const baseUrl = process.env.BASE_URL || 'http://localhost:3030';

export const validateText = (text) => {
  return (typeof text === 'string' && text.length > 0);
};
