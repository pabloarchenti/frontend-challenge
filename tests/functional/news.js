/* eslint-disable func-call-spacing, no-spaced-func */
/* global fixture test */
import { baseUrl } from './common';
import NewsPage from './pages/news';

fixture('News page');

test.page`${baseUrl}` ('User should be able to browse articles on the homepage', async (t) => {
  const newsPage = await NewsPage.create();
  await newsPage.isValid();
  const prevArticlesLength = newsPage.getArticlesLength();
  await newsPage.loadMore();
  await newsPage.isValid();
  await t.expect(prevArticlesLength)
    .lt(newsPage.getArticlesLength(), 'No more articles couldn\'t be loaded');
});

test.page`${baseUrl}/news/technology` ('User should be able to browse articles filtered by category on the url', async () => {
  const newsPage = await NewsPage.create();
  await newsPage.isValid();
});

test.page`${baseUrl}` ('User should be able to filter articles using the categories menu', async () => {
  await NewsPage.goToCategory('health');
  const newsPage = await NewsPage.create();
  await newsPage.isValid();
});
