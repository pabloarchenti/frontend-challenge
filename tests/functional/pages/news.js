/* eslint-disable class-methods-use-this, no-await-in-loop */
import { Selector, t } from 'testcafe';
import { validateText } from '../common';

class Article {
  constructor() {
    this.title = null;
    this.subtitle = null;
    this.image = null;
  }

  isValid() {
    if (!validateText(this.title)) {
      throw new Error(`Article's title is invalid: "${this.title}"`);
    }
    if (!validateText(this.subtitle)) {
      throw new Error(`Article's subtitle is invalid': "${this.subtitle}"`);
    }
    if (!validateText(this.image)) {
      throw new Error(`Article's image is invalid: "${this.imageSrc}"`);
    }
    return true;
  }

  static async create(element) {
    const article = new Article();

    const titleElement = await element.find('h2');
    article.title = await titleElement.innerText;

    const subtitleElement = await element.find('h3');
    article.subtitle = await subtitleElement.innerText;

    const imageElement = await element.find('img');
    article.image = await imageElement.getAttribute('src');

    return article;
  }
}

export default class NewsPage {
  constructor() {
    this.articles = [];
  }

  async isValid() {
    await t.expect(this.getArticlesLength()).gt(0, 'News page has no articles');
    this.articles.forEach(article => article.isValid());
  }

  async loadMore() {
    await t.click(Selector('#load-more-button'));
    const moreArticles = await NewsPage.getArticlesFromPosition(this.articles.length);
    this.articles = this.articles.concat(moreArticles);
  }

  getArticles() {
    return this.articles;
  }

  getArticlesLength() {
    return this.getArticles().length;
  }

  static async create() {
    const newsPage = new NewsPage();
    newsPage.articles = await NewsPage.getArticlesFromPosition(0);
    return newsPage;
  }

  static async getArticlesFromPosition(position) {
    const articlesElem = Selector('article');
    const articles = [];
    const count = await articlesElem.count;
    for (let i = position; i < count; i++) {
      articles.push(await Article.create(articlesElem.nth(i)));
    }
    return articles;
  }

  static async goToCategory(category) {
    await t.click(Selector(`#${category}-menu-link`));
  }
}
