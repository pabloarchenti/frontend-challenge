import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import getConfig from 'next/config';
import Head from 'next/head';
import theme from '../theme';
import { getArticles } from '../actions/news';
import ArticlesList from '../components/ArticlesList';
import CategoriesMenu from '../components/CategoriesMenu';
import { capitalize } from '../helpers/string';

const { publicRuntimeConfig: publicConfig } = getConfig();

const START_PAGE = 1;
const PAGE_SIZE = 12;

class NewsPage extends React.Component {
  static getInitialProps = async ({ query, reduxStore }) => {
    const { category, pageSize = PAGE_SIZE } = query;
    const { categories } = publicConfig;
    const page = START_PAGE;

    return reduxStore.dispatch(getArticles(page, pageSize, category))
      .then(() => ({
        categories,
        category,
        pageSize
      }));
  }

  static propTypes = {
    categories: PropTypes.arrayOf(PropTypes.string).isRequired,
    category: PropTypes.string,
    articles: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        imageUrl: PropTypes.string,
        date: PropTypes.string,
        source: PropTypes.string
      })
    ),
    getArticles: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    isFetching: PropTypes.bool,
    errorFetching: PropTypes.bool
  };

  static defaultProps = {
    category: '',
    articles: [],
    isFetching: false,
    errorFetching: false
  }

  onLoadMore = () => {
    const { getArticles, page, category, pageSize } = this.props;
    getArticles(page + 1, pageSize, category);
  }

  render() {
    const { categories, category, articles, errorFetching, isFetching } = this.props;
    return (
      <div>
        <Head>
          <title className="pageTitle">World news - {capitalize(category) || 'Home'}</title>
        </Head>
        <div className="container">
          <CategoriesMenu category={category} categories={categories} />
          <section id="content">
            <ArticlesList
              articles={articles}
              errorFetching={errorFetching}
              isFetching={isFetching}
              onLoadMore={this.onLoadMore}
            />
          </section>
          <style jsx>{`
            .container {
              display: flex;
              max-width: 1400px;
              margin: 0 auto;
              padding: ${theme.spacing(3)}px;
              padding-top: ${theme.spacing(20)}px
            }
          `}
          </style>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  articles: state.news.articles,
  page: state.news.page,
  isFetching: state.news.isFetching,
  errorFetching: state.news.error !== null
});

export default connect(
  mapStateToProps,
  { getArticles }
)(NewsPage);
