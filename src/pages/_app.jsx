import React from 'react';
import App, { Container } from 'next/app';
import { Provider as StoreProvider } from 'react-redux';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import withReduxStore from '../hocs/withReduxStore';
import theme from '../theme';

class MyApp extends App {
  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component: Page, pageProps: props, reduxStore } = this.props;
    return (
      <Container>
        <StoreProvider store={reduxStore}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Page {...props} />
          </ThemeProvider>
        </StoreProvider>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);
