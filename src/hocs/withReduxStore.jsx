import React from 'react';
import PropTypes from 'prop-types';
import initializeStore from '../store';

const isServer = typeof window === 'undefined';
const _REDUX_STORE_ = '__NEXT_REDUX_STORE__';

function getOrCreateStore(initialState) {
  if (isServer) {
    return initializeStore(initialState);
  }

  if (!window[_REDUX_STORE_]) {
    window[_REDUX_STORE_] = initializeStore(initialState);
  }

  return window[_REDUX_STORE_];
}

export default function (App) {
  return class AppWithRedux extends React.Component {
    static propTypes = {
      initialReduxState: PropTypes.object.isRequired // eslint-disable-line
    };

    constructor(props) {
      super(props);
      this.reduxStore = getOrCreateStore(props.initialReduxState);
    }

    static async getInitialProps(appContext) {
      const reduxStore = getOrCreateStore();

      appContext.ctx.reduxStore = reduxStore;

      let appProps = {};
      if (typeof App.getInitialProps === 'function') {
        appProps = await App.getInitialProps(appContext);
      }

      return {
        ...appProps,
        initialReduxState: reduxStore.getState()
      };
    }

    render() {
      return <App {...this.props} reduxStore={this.reduxStore} />;
    }
  };
}
