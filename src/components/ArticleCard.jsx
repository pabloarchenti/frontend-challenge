import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import TimeAgo from 'react-timeago';
import LazyLoad from 'react-lazyload';

const IMG_HEIGHT = 250;
const IMG_OFFSET = 100;

const useStyles = makeStyles({
  card: {
    minWidth: 350
  },
  media: {
    height: IMG_HEIGHT
  },
  content: {
    cursor: 'auto'
  }
});

const ArticleCard = ({
  title, description, imageUrl, date, source
}) => {
  const classes = useStyles();

  return (
    <article>
      <Card className={classes.card}>
        <CardActionArea>
          <LazyLoad height={IMG_HEIGHT} offset={IMG_OFFSET} placeholder={(<img src="/images/loading.gif" height={IMG_HEIGHT} alt="" />)} resize once>
            <CardMedia
              component="img"
              className={classes.media}
              image={imageUrl}
              title={title}
            />
          </LazyLoad>
          <CardContent className={classes.content}>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="h3">
              {description}
            </Typography>
            {
              source && date && (
                <Typography variant="body2" align="right">
                  {source} - <TimeAgo date={date} />
                </Typography>
              )
            }
          </CardContent>
        </CardActionArea>
      </Card>
    </article>
  );
};

ArticleCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  date: PropTypes.string,
  source: PropTypes.string
};

ArticleCard.defaultProps = {
  date: null,
  source: null
};

export default ArticleCard;
