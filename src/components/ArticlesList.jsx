
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import ArticleCard from './ArticleCard';

const useStyles = makeStyles(theme => ({
  container: {
    textAlign: 'center',
    flexGrow: 1,
  },
  loadMoreContainer: {
    textAlign: 'center',
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(10)
  },
  progress: {
    margin: theme.spacing(5)
  },
  progress2: {
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 5000,
    width: '100%'
  }
}));

const ArticlesList = ({ articles, errorFetching, isFetching, onLoadMore }) => {
  const classes = useStyles();

  const renderCards = () => {
    return articles.map((article) => {
      return (
        <Grid item xs key={article.title}>
          <ArticleCard
            title={article.title}
            description={article.description}
            imageUrl={article.imageUrl}
            date={article.date}
            source={article.source}
          />
        </Grid>
      );
    });
  };

  return (
    <div className={classes.container}>
      <Grid container spacing={2} justify="center" alignContent="center">
        {renderCards()}
      </Grid>
      {
        errorFetching
        && (<div>An error has occurred fetching the news...</div>)
      }
      {
        isFetching
        && (
          <div>
            <CircularProgress color="primary" className={classes.progress} />
            <LinearProgress color="primary" className={classes.progress2} />
          </div>
        )
      }
      <div className={classes.loadMoreContainer} id="load-more-button">
        <Button variant="contained" color="primary" onClick={onLoadMore}>
          Load more
        </Button>
      </div>
    </div>
  );
};

ArticlesList.propTypes = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      imageUrl: PropTypes.string,
      date: PropTypes.string,
      source: PropTypes.string
    })
  ).isRequired,
  errorFetching: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  onLoadMore: PropTypes.func.isRequired
};

export default ArticlesList;
