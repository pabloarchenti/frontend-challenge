import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';

const drawerWidth = 200;

const useStyles = makeStyles(theme => ({
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    }
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  category: {
    paddingRight: theme.spacing(4),
    textTransform: 'capitalize'
  },
  navBarText: {
    textTransform: 'capitalize'
  }
}));

const CategoriesMenu = ({ categories, category }) => {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [selectedMenuItem, setSelectedMenuItem] = React.useState(
    categories.findIndex(elem => elem === category) + 1
  );

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handlerMenuItemClick = (event, index) => {
    if (mobileOpen) {
      handleDrawerToggle();
    }
    setSelectedMenuItem(index);
  };

  const renderNavigationBarItem = (category, link, index) => (
    <Link href={`/news?category=${link}`} as={`/news/${link}`} key={category}>
      <ListItem
        button
        onClick={event => handlerMenuItemClick(event, index)}
        selected={selectedMenuItem === index}
        id={`${category}-menu-link`}
      >
        <ListItemText primary={category} className={classes.navBarText} />
      </ListItem>
    </Link>
  );

  const navigationBarContent = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {renderNavigationBarItem('home', '', 0)}
        {categories.map(
          (category, index) => renderNavigationBarItem(category, category, index + 1)
        )}
      </List>
    </div>
  );

  const topBar = (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap className={classes.category}>
          {category || 'Home'}
        </Typography>
      </Toolbar>
    </AppBar>
  );

  const navigationBar = (
    <nav className={classes.drawer}>
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          anchor="left"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true,
          }}
        >
          {navigationBarContent}
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {navigationBarContent}
        </Drawer>
      </Hidden>
    </nav>
  );

  return (
    <React.Fragment>
      {topBar}
      {navigationBar}
    </React.Fragment>
  );
};

CategoriesMenu.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.string).isRequired,
  category: PropTypes.string
};

CategoriesMenu.defaultProps = {
  category: ''
};

export default CategoriesMenu;
