import { types } from '../actions/news';

const initialState = {
  articles: [],
  page: 1,
  isFetching: false,
  error: null
};

export const news = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case types.NEWS_SUCCESS:
      return {
        articles: action.data,
        page: 1,
        isFetching: false,
        error: null
      };
    case types.MORE_SUCCESS:
      return {
        articles: state.articles.concat(action.data),
        page: action.page,
        isFetching: false,
        error: null
      };
    case types.FAILURE:
      return {
        ...state,
        error: action.data
      };
    default:
      return state;
  }
};
