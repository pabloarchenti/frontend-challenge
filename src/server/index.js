/* eslint-disable no-console */

const express = require('express');
const nextJS = require('next');
const compression = require('compression');
const morgan = require('morgan');
const { join } = require('path');
const config = require('../../config/server');
const middlewares = require('./middlewares');

const PORT = parseInt(process.env.PORT, 10) || config.port;
const dev = process.env.NODE_ENV !== 'production';
const app = nextJS({ dev, dir: 'src' });

app.prepare().then(() => {
  const server = express();

  server.use(morgan('combined'));

  server.use(compression());

  server.get('/api/news/:category?', middlewares.api(config.newsApi));

  server.get('/service-worker.js', (req, res) => {
    const filePath = join(__dirname, '../../.build/service-worker.js');
    app.serveStatic(req, res, filePath);
  });

  server.use(express.static('public'));

  server.get('/news/:category?', middlewares.news(app));
  server.get('*', middlewares.news(app));

  server.listen(PORT, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${PORT}`);
  });
});
