const fetch = require('isomorphic-unfetch');
const querystring = require('querystring');

module.exports = config => (req, res) => {
  const maxCache = config.maxCache || 60 * 5;
  const qs = {
    ...req.query,
    country: config.country,
  };

  if (req.params.category) {
    qs.category = req.params.category;
  }

  fetch(
    `${config.url}?${querystring.stringify(qs)}`,
    {
      headers: {
        'X-Api-Key': config.key
      }
    }
  ).then((upstreamRes) => {
    res.set('cache-control', `max-age=${maxCache} private`);
    res.set('content-type', upstreamRes.headers.get('content-type'));
    upstreamRes.body.pipe(res);
  }).catch((err) => {
    res.status(500).json({
      status: 'error',
      code: 'fetchError',
      message: err.message
    });
  });
};
