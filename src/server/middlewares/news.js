module.exports = app => (req, res) => app.render(req, res, '/news', { ...req.query, category: req.params.category });
