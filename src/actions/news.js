
import fetch from 'isomorphic-unfetch';
import getConfig from 'next/config';

const { serverRuntimeConfig: serverConfig } = getConfig();
const baseUrl = serverConfig.port ? `http://localhost:${serverConfig.port}` : '';

export const types = {
  REQUEST: 'FETCH_NEWS_REQUEST',
  NEWS_SUCCESS: 'FETCH_NEWS_SUCCESS',
  MORE_SUCCESS: 'FETCH_MORE_SUCCESS',
  FAILURE: 'FETCH_NEWS_FAILURE'
};

export const getArticles = (page, pageSize, category = '') => {
  return (dispatch) => {
    dispatch({
      type: types.REQUEST
    });

    return fetch(`${baseUrl}/api/news/${category}?page=${page}&pageSize=${pageSize}`)
      .then(
        response => response.json(),
        err => (dispatch({
          type: types.FAILURE,
          data: err
        }))
      )
      .then((json) => {
        if (json.status === 'ok') {
          const articles = json.articles
            .map(article => ({
              title: article.title,
              description: article.description,
              imageUrl: article.urlToImage,
              date: article.publishedAt,
              source: article.source.name
            }))
            .filter(article => (
              article.title !== null
              && article.description !== null
              && article.imageUrl !== null
            ));

          if (page > 1) {
            dispatch({
              type: types.MORE_SUCCESS,
              data: articles,
              page
            });
          } else {
            dispatch({
              type: types.NEWS_SUCCESS,
              data: articles
            });
          }
        } else {
          dispatch({
            type: types.FAILURE,
            data: new Error(json.code)
          });
        }
      });
  };
};
