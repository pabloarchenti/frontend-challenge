FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

ARG PORT
ENV PORT $PORT

EXPOSE $PORT
CMD [ "npm", "start" ]
